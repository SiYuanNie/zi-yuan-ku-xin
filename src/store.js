import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
    loading: false
}

const mutations = {
    // loading预加载
    changeLoading(state, flag) {
        state.loading = flag;
    },
}

export default new Vuex.Store({
    state,
    mutations
})