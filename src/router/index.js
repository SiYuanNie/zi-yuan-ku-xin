import Vue from 'vue'
import Router from 'vue-router'
import login from '../components/userinfo/login'                    //登录
import agreementFW from '../components/userinfo/agreementFW'      	//服务协议
import agreementYS from '../components/userinfo/agreementYS'      	//隐私协议
import agreementZYSC from '../components/userinfo/agreementZYSC'    //资源上传协议
import forgetPsd from '../components/userinfo/forgetPsd'            //忘记密码
import sign from '../components/userinfo/sign'                      //注册
import userinfo from '../components/userinfo/userinfo'              //我的
import index from '../components/userinfo/index'                    //首页
import teaStudy from '../components/userinfo/teaStudy'              //教研
import userData from '../components/userData/userData'              //个人资料
import replacePhone from '../components/userData/replacePhone'      //更换手机号
import verPhone from '../components/userData/verPhone'      		//验证手机号
import news from '../components/userinfo/news'                      //消息
import set from '../components/userinfo/set'                        //设置
import modifyPsd from '../components/userinfo/modifyPsd'            //修改密码
import myUp from '../components/userinfo/myUp'                      //我的上传
import certification from '../components/userinfo/certification'    //实名认证
import seeReason from '../components/userinfo/seeReason'    		//查看原因
import szRecord from '../components/userinfo/szRecord'              //收支记录
import payOrder from '../components/userinfo/payOrder'           	//支付订单
import colAccount from '../components/userinfo/colAccount'          //收款账户
import incomeState from '../components/userinfo/incomeState'        //收入声明
import expRecord from '../components/userinfo/expRecord'            //经验值记录
import upload from '../components/upload/upload'                    //上传资源
import upCover from '../components/upload/upCover'                  //封面上传
import wkResource from '../components/rousource/wkResource'         //微课资源
import seeResourse from '../components/rousource/seeResourse'       //查看资源（付费/免费）
import zyDetail from '../components/rousource/zyDetail'             //资源详情
import reInteraction from '../components/rousource/reInteraction'   //资源互动
import discussDetail from '../components/rousource/discussDetail'   //评论详情
import taskAsk from '../components/rousource/taskAsk'               //作业要求
import askExplain from '../components/rousource/askExplain'         //要求说明
import teaRemark from '../components/rousource/teaRemark'           //教师评语
import actInfo from '../components/rousource/actInfo'               //活动信息
import report from '../components/rousource/report'                 //举报
import step from '../components/rousource/step'                     //步骤
import coopInfo from '../components/rousource/coopInfo'             //协作信息
import theme from '../components/rousource/theme'                   //主题
import commonSearch from '../components/common/commonSearch'        //搜索
import commentList from '../components/comment/commentList'         //评论列表
import commentDetail from '../components/comment/commentDetail'     //评论详情



Vue.use(Router)

export default new Router({
	routes: [
		{
			path: '/',
			name: 'index',
			component: index
		},
		{
			path: '/login',
			name: 'login',
			component: login
		},

		{
			path: '/agreementFW',
			name: 'agreementFW',
			component: agreementFW
		},
		{
			path: '/agreementYS',
			name: 'agreementYS',
			component: agreementYS
		},

		{
			path: '/agreementZYSC',
			name: 'agreementZYSC',
			component: agreementZYSC
		},
		{
			path: '/forgetPsd',
			name: 'forgetPsd',
			component: forgetPsd
		},
		{
			path: '/sign',
			name: 'sign',
			component: sign
		},
		{
			path: '/userinfo',
			name: 'userinfo',
			meta: { keepAlive: false },
			component: userinfo
		},
		{
			path: '/index',
			name: 'index',
			component: index
		},
		{
			path: '/teaStudy',
			name: 'teaStudy',
			component: teaStudy
		},
		{
			path: '/userData',
			name: 'userData',
			component: userData
		},
		{
			path: '/replacePhone',
			name: 'replacePhone',
			component: replacePhone
		},
		{
			path: '/verPhone',
			name: 'verPhone',
			component: verPhone
		},
		{
			path: '/news',
			name: 'news',
			component: news
		},
		{
			path: '/set',
			name: 'set',
			component: set
		},
		{
			path: '/modifyPsd',
			name: 'modifyPsd',
			component: modifyPsd
		},
		{
			path: '/myUp',
			name: 'myUp',
			component: myUp
		},
		{
			path: '/certification',
			name: 'certification',
			component: certification
		},
		{
			path: '/seeReason',
			name: 'seeReason',
			component: seeReason
		},
		{
			path: '/szRecord',
			name: 'szRecord',
			component: szRecord
		},
		{
			path: '/payOrder',
			name: 'payOrder',
			component: payOrder
		},
		{
			path: '/colAccount',
			name: 'colAccount',
			component: colAccount
		},
		{
			path: '/incomeState',
			name: 'incomeState',
			component: incomeState
		},
		{
			path: '/expRecord',
			name: 'expRecord',
			component: expRecord
		},
		{
			path: '/upload',
			name: 'upload',
			component: upload
		},
		{
			path: '/upCover',
			name: 'upCover',
			component: upCover
		},
		{
			path: '/wkResource',
			name: 'wkResource',
			component: wkResource
		},
		{
			path: '/seeResourse',
			name: 'seeResourse',
			component: seeResourse
		},
		{
			path: '/zyDetail',
			name: 'zyDetail',
			component: zyDetail
		},
		{
			path: '/reInteraction',
			name: 'reInteraction',
			component: reInteraction
		},
		{
			path: '/discussDetail',
			name: 'discussDetail',
			component: discussDetail
		},
		{
			path: '/taskAsk',
			name: 'taskAsk',
			component: taskAsk
		},
		{
			path: '/askExplain',
			name: 'askExplain',
			component: askExplain
		},
		{
			path: '/teaRemark',
			name: 'teaRemark',
			component: teaRemark
		},
		{
			path: '/actInfo',
			name: 'actInfo',
			component: actInfo
		},
		{
			path: '/step',
			name: 'step',
			component: step
		},
		{
			path: '/coopInfo',
			name: 'coopInfo',
			component: coopInfo
		},
		{
			path: '/theme',
			name: 'theme',
			component: theme
		},
		{
			path: '/report',
			name: 'report',
			component: report
		},
		{
			path: '/commonSearch',
			name: 'commonSearch',
			component: commonSearch
		},
		{
			path: '/commentList',
			name: 'commentList',
			component: commentList
		},
		{
			path: '/commentDetail',
			name: 'commentDetail',
			component: commentDetail
		},
	]
})
