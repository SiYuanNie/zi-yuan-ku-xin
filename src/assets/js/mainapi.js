import siteconfig from "./api";
import axios from "axios"

var URL = siteconfig.api.yxt();
// var URL = 'api/';
var url_login = siteconfig.api.login;
var url_long = siteconfig.api.long;
var url_focus = siteconfig.api.focus;


function mainapi(name, obj, callback) {
    if (callback === undefined) callback = function () { };

    switch (name) {
        // 登录相关接口
        //用户登录
        case 'user_Login':
            request(url_long + '/resourcelib/resCommon/login.json', obj, callback, 'POST', true);
            break;
        //用户注册接口
        case 'resLibRegister':
            request('/projAjax/resLibRegister.json', obj, callback, 'POST', true);
            break;
        //获取验证码
        case 'sendPhone':
            request(url_long + '/resourcelib/resCommon/sendPhone.json', obj, callback, 'POST', true);
            break;
        //根据验证码修改密码
        case 'updatePwdByRank':
            request('/projAjax/updatePwdByRank.json', obj, callback, 'POST', true);
            break;
        //用户修改密码
        case 'updatePwd':
            request('projAjax/updatePwd.json', obj, callback, 'POST', true);
            break;
        //我的首页信息
        case 'getMyIndex':
            request(url_long + '/resourcelib/resSearch/getMyIndex.json', obj, callback, 'POST', true);
            break;
        //获取用户个人资料
        case 'getUserInfo':
            request(url_long + '/resourcelib/resCommon/getUserInfo.json', obj, callback, 'POST', true);
            break;
        //修改个人资料
        case 'updateUserInfo':
            request('/mobile/resAppUser/updateUserInfo.json', obj, callback, 'POST', true);
            break;
        // 用户修改头像
        case 'updateResLibUserPic':
            request('/projAjax/updateResLibUserPic.json', obj, callback, 'POST', true);
            break;
        //获取实名认证信息
        case 'getAuthentication':
            request(url_long + '/resourcelib/resCommon/getAuthentication.json', obj, callback, 'POST', true);
            break;
        //保存用户实名认证信息
        case 'saveAuthentication':
            request(url_long + '/resourcelib/resCommon/saveAuthentication.json', obj, callback, 'POST', true);
            break;
        //获取用户收款账号信息
        case 'getAccount':
            request(url_long + '/resourcelib/resCommon/getAccount.json', obj, callback, 'POST', true);
            break;
        //用户设置收款账户
        case 'setAccount':
            request(url_long + '/resourcelib/resCommon/setAccount.json', obj, callback, 'POST', true);
            break;
        //获取用户经验值记录
        case 'getExpRecord':
            request(url_long + '/resourcelib/resSearch/getExpRecord.json', obj, callback, 'POST', true);
            break;
        //获取用户收入记录列表
        case 'getIncomeRecord':
            request(url_long + '/resourcelib/resSearch/getIncomeRecord.json', obj, callback, 'POST', true);
            break;
        //获取用户支出记录列表
        case 'getDisburseRecord':
            request(url_long + '/resourcelib/resSearch/getDisburseRecord.json', obj, callback, 'POST', true);
            break;
        //用户删除订单
        case 'delAppOrder':
            request(url_long + '/resourcelib/resCommon/delAppOrder.json', obj, callback, 'POST', true);
            break;
        //用户取消订单
        case 'cancelOrder':
            request(url_long + '/resourcelib/resCommon/cancelOrder.json', obj, callback, 'POST', true);
            break;
        //我的资源_我的上传
        case 'getMyUpload':
            request(url_long + '/resourcelib/resSearch/getMyUpload.json', obj, callback, 'POST', true);
            break;
        //用户删除资源
        case 'delDocument':
            request(url_long + '/resourcelib/resCommon/delDocument.json', obj, callback, 'POST', true);
            break;
        //我的资源_我的收藏
        case 'getMyCollection':
            request(url_long + '/resourcelib/resSearch/getMyCollection.json', obj, callback, 'POST', true);
            break;
        //取消收藏
        case 'cancelCollect':
            request(url_long + '/resourcelib/resCommon/cancelCollect.json', obj, callback, 'POST', true);
            break;
        //我的资源_我的购买
        case 'getMyPurchase':
            request(url_long + '/resourcelib/resSearch/getMyPurchase.json', obj, callback, 'POST', true);
            break;
        //我的资源_我的下载
        case 'getMyDownload':
            request(url_long + '/resourcelib/resSearch/getMyDownload.json', obj, callback, 'POST', true);
            break;
        //查看消息_获取资源评论接口
        case 'getMyComment':
            request(url_long + '/resourcelib/resSearch/getMyComment.json', obj, callback, 'POST', true);
            break;
        //查看消息_获取@我的接口
        case 'getAtMe':
            request(url_long + '/resourcelib/resSearch/getAtMe.json', obj, callback, 'POST', true);
            break;
        //查看消息_获取动态消息接口
        case 'getDynamicMessage':
            request(url_long + '/resourcelib/resSearch/getDynamicMessage.json', obj, callback, 'POST', true);
            break;
        //用户删除动态消息
        case 'delDynamicMessage':
            request(url_long + '/resourcelib/resCommon/delDynamicMessage.json', obj, callback, 'POST', true);
            break;
        //获取首页信息
        case 'getIndexList':
            request(url_long + '/resourcelib/resSearch/getIndexList.json', obj, callback, 'POST', true);
            break;
        //教研_获取精选微课
        case 'getMicroShow':
            request(url_long + '/resourcelib/resSearch/getMicroShow.json', obj, callback, 'POST', true);
            break;
        //教研_获取付费精选
        case 'getPayIndexShow':
            request(url_long + '/resourcelib/resSearch/getPayIndexShow.json', obj, callback, 'POST', true);
            break;
        //教研_获取优秀资源
        case 'getResourceShow':
            request(url_long + '/resourcelib/resSearch/getResourceShow.json', obj, callback, 'POST', true);
            break;
        //教研_获取研修成果
        case 'getResearchResultShow':
            request(url_long + '/resourcelib/resSearch/getResearchResultShow.json', obj, callback, 'POST', true);
            break;
        //获取微课资源列表
        case 'getResMicro':
            request(url_long + '/resourcelib/resSearch/getResMicro.json', obj, callback, 'POST', true);
            break;
        //获取教学资源列表
        case 'getTeacherRes':
            request(url_long + '/resourcelib/resSearch/getTeacherRes.json', obj, callback, 'POST', true);
            break;
        //获取研修资源列表
        case 'getResTraining':
            request(url_long + '/resourcelib/resSearch/getResTraining.json', obj, callback, 'POST', true);
            break;
        //查看微课资源
        case 'getMicro':
            request(url_long + '/resourcelib/resAppDetail/getMicro.json', obj, callback, 'POST', true);
            break;
        //查看教学资源
        case 'getTeacherLib':
            request(url_long + '/resourcelib/resAppDetail/getTeacherLib.json', obj, callback, 'POST', true);
            break;
        //查看文章资源
        case 'getArticle':
            request(url_long + '/resourcelib/resAppDetail/getArticle.json', obj, callback, 'POST', true);
            break;
        //查看作业
        case 'getUsertask':
            request(url_long + '/resourcelib/resAppDetail/getUsertask.json', obj, callback, 'POST', true);
            break;
        //查看活动
        case 'getActivity':
            request(url_long + '/resourcelib/resAppDetail/getActivity.json', obj, callback, 'POST', true);
            break;
        //查看同伴协作
        case 'getCooperate':
            request(url_long + '/resourcelib/resAppDetail/getCooperate.json', obj, callback, 'POST', true);
            break;
        //查看交流
        case 'getCommunicate':
            request(url_long + '/resourcelib/resAppDetail/getCommunicate.json', obj, callback, 'POST', true);
            break;
        //获取分类体系(学段学科)字典
        case 'getSolrDictList':
            request(url_long + '/resourcelib/resCommon/getSolrDictList.json', obj, callback, 'POST', true);
            break;
        //查看详情
        case 'getDetail':
            request(url_long + '/resourcelib/resAppDetail/getDetail.json', obj, callback, 'POST', true);
            break;
        //资源推荐接口
        case 'getRecommend':
            request(url_long + '/resourcelib/resSearch/getRecommend.json', obj, callback, 'POST', true);
            break;
        //用户收藏资源
        case 'addCollect':
            request(url_long + '/resourcelib/resCommon/addCollect.json', obj, callback, 'POST', true);
            break;
        //资源评价
        case 'addGrade':
            request(url_long + '/resourcelib/resCommon/addGrade.json', obj, callback, 'POST', true);
            break;
        //资源举报
        case 'reportRes':
            request(url_long + '/resourcelib/resCommon/reportRes.json', obj, callback, 'POST', true);
            break;
        //研修资源互动评论
        case 'getComments':
            request(url_long + '/resourcelib/resAppDetail/getComments.json', obj, callback, 'POST', true);
            break;
        //研修资源评论详情
        case 'getReplys':
            request(url_long + '/resourcelib/resAppDetail/getReplys.json', obj, callback, 'POST', true);
            break;
        //作业要求
        case 'getTaskRequirement':
            request(url_long + '/resourcelib/resAppDetail/getTaskRequirement.json', obj, callback, 'POST', true);
            break;
        //作业教师评语
        case 'getTaskEvaluation':
            request(url_long + '/resourcelib/resAppDetail/getTaskEvaluation.json', obj, callback, 'POST', true);
            break;
        //查看活动信息
        case 'getActivityMsg':
            request(url_long + '/resourcelib/resAppDetail/getActivityMsg.json', obj, callback, 'POST', true);
            break;
        //查看活动步骤
        case 'getActivityStep':
            request(url_long + '/resourcelib/resAppDetail/getActivityStep.json', obj, callback, 'POST', true);
            break;
        //查看活动评论
        case 'getActivityComments':
            request(url_long + '/resourcelib/resAppDetail/getActivityComments.json', obj, callback, 'POST', true);
            break;
        //查看同伴协作信息
        case 'getCooperateMsg':
            request(url_long + '/resourcelib/resAppDetail/getCooperateMsg.json', obj, callback, 'POST', true);
            break;
        //查看同伴协作主题
        case 'getCooperateTitle':
            request(url_long + '/resourcelib/resAppDetail/getCooperateTitle.json', obj, callback, 'POST', true);
            break;
        //获取同伴协作主题下的步骤
        case 'getCooperateStep':
            request(url_long + '/resourcelib/resAppDetail/getCooperateStep.json', obj, callback, 'POST', true);
            break;
        //同伴协作获取评论信息
        case 'getCooperateComments':
            request(url_long + '/resourcelib/resAppDetail/getCooperateComments.json', obj, callback, 'POST', true);
            break;
        // 资源评论_获取资源评论列表
        case 'getResComments':
            request(url_long + '/resourcelib/resSearch/getResComments.json', obj, callback, 'POST', true);
            break;
        // 资源评论_获取评论下的回复
        case 'getResReplys':
            request(url_long + '/resourcelib/resSearch/getResReplys.json', obj, callback, 'POST', true);
            break;
        // 资源评论删除评论
        case 'delAppComment':
            request(url_long + '/resourcelib/resCommon/delAppComment.json', obj, callback, 'POST', true);
            break;
        // 资源评论_添加评论
        case 'addComment':
            request(url_long + '/resourcelib/resCommon/addComment.json', obj, callback, 'POST', true);
            break;
        // 资源评论_删除回复
        case 'delReply':
            request(url_long + '/resourcelib/resCommon/delReply.json', obj, callback, 'POST', true);
            break;
        // 资源回复_添加回复
        case 'addReply':
            request(url_long + '/resourcelib/resCommon/addReply.json', obj, callback, 'POST', true);
            break;
        // 资源评论_举报评论
        case 'reportComment':
            request(url_long + '/resourcelib/resCommon/reportComment.json', obj, callback, 'POST', true);
            break;
        // 资源评论_举报回复
        case 'reportReply':
            request(url_long + '/resourcelib/resCommon/reportReply.json', obj, callback, 'POST', true);
            break;
        // 获取资源搜索列表
        case 'getSearch':
            request(url_long + '/resourcelib/resSearch/getSearch.json', obj, callback, 'POST', true);
            break;
        // 获取分类
        case 'getClassify':
            request(url_long + '/resourcelib/resCommon/getClassify.json', obj, callback, 'POST', true);
            break;
        // 获取用户订单
        case 'getOrderInfo':
            request(url_long + '/resourcelib/resCommon/getOrderInfo.json', obj, callback, 'POST', true);
            break;
        // 微信支付
        case 'weChatPagePay':
            request(url_long + '/resourcelib/resAppDetail/weChatPagePay.json', obj, callback, 'POST', true);
            break;
        // 支付宝支付
        case 'pageAliPay':
            request(url_long + '/resourcelib/resAppDetail/pageAliPay.json', obj, callback, 'POST', true);
            break;
        // 上传教学资源  addLibVideo
        case 'addResource':
            request(url_long + '/resourcelib/resCommon/addResource.json', obj, callback, 'POST', true);
            break;
        // 创建视频信息
        case 'createVideoUploadInfo':
            request(url_long + '/resourcelib/resCCUploadAjax/createVideoUploadInfo.json', obj, callback, 'POST', true);
            break;
        // 视频上传到cc服务器调本接口
        case 'submitCC':
            request(url_long + '/resourcelib/resCCUploadAjax/submit.json', obj, callback, 'POST', true);
            break;
        // 上传微课资源
        case 'addLibVideo':
            request(url_long + '/resourcelib/resCCUploadAjax/addLibVideo.json', obj, callback, 'POST', true);
            break;
        // 用户下载教学资源时保存下载记录
        case 'addDownLoadCount':
            request(url_long + '/resourcelib/resAppDetail/addDownLoadCount.json', obj, callback, 'POST', true);
            break;
        default:
            break;
    }
}

function request(url, obj, callback, type, isAsync) {
    if (callback === undefined) callback = function () { };
    if (!isAsync) isAsync = true;
    // var type = type ? type : 'GET';

    var params = {
        // headers: {
        // 	'Content-Type': 'multipart/form-data'
        // },
        method: type,
        url: URL + url,
        transformRequest: [function (data) {			//转对象格式
            let ret = ''
            for (let it in data) {
                ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
            }
            return ret
        }],
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
            // 'Content-Type': 'application/json'
        }
    }
    if (type == 'GET') {
        params.params = obj;
    } else {
        params.data = obj;
    }
    // var params = JSON.params(params)
    axios(params).then(function (res) {
        console.log("成功")
        callback(res)
        if (res.data.status == "FAIL") {
            if (res.data.message == "缺少登录用户!") {
                // qq内置浏览器判断
                var isIosQQ = ( /(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent) && /\sQQ/i.test(navigator.userAgent));
                var isAndroidQQ = ( /(Android)/i.test(navigator.userAgent) && /MQQBrowser/i.test(navigator.userAgent) && /\sQQ/i.test((navigator.userAgent).split('MQQBrowser')));
                if(isIosQQ){
                    alert('请在微信内环境或其他浏览器进行访问')
                }
                // qq内置浏览器判断end
                var sure = confirm("登录后即可观看,是否去登录")
                if (sure) {
                    window.location.href = '#/login'
                } else {
                    window.history.go(-1)
                }
            }
        }
    }).catch(function (res) {
        console.log("系统错误")
        console.log(res)
    });


}
export default mainapi;
