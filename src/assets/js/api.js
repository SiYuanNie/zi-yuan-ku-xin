var siteconfig = {};
var allConfig = {
	mtconfig: {
		api: {
			// yxt: "api/",			 						//本地开发
			// yxt:"https://yxt.teacheredu.cn/",			//https
			// yxt:"http://cas.cs.teacheredu.cn/",			//cs
			// yxt:"http://cas.cspub.teacheredu.cn/",		//cspub
			// yxt:"http://cas.study.teacheredu.cn/",		//外网

			yxt: function () {
                var URL = window.location.href
                return 'http://cas.cspub.teacheredu.cn/'       //本地测试用
                // return 'http://cas.dev.teacheredu.cn/'       //230
                if (URL.indexOf("cspub") > 0) { //1cspub环境
                    return 'http://cas.cspub.teacheredu.cn/'
                } else if (URL.indexOf("cs") > 0) {// 2cs环境
                    return 'http://cas.cs.teacheredu.cn/'
                } else if (URL.indexOf("history") > 0) { //3历史环境
                    return 'http://cas.history.teacheredu.cn/'
                } else if (URL.indexOf("jszgz") > 0) {//4教师资格环境
                    return 'http://cas.jszgz.teacheredu.cn/'
                } else if (URL.indexOf("teacheredu") > 0) { //5教师网外网
                    return 'http://cas.study.teacheredu.cn/'
                } else if (URL.indexOf("yanxiu") > 0) { //6中国教育电视台
                    return 'http://cas.study.yanxiu.jsyxsq.com/'
                } else if (URL.indexOf("xjnuteacher") > 0) { //7新疆师范大学
                    return 'http://cas.xjnuteacher.com/'
                } else if (URL.indexOf("nbucec") > 0) {//8宁波大学
                    return 'http://jspx.nbucec.com/'
                } else if (URL.indexOf("sxsd") > 0) {//9陕西师大
                    return 'http://sxsd.01601.cn/'
                } else if (URL.indexOf("01601") > 0) {//10洮北（其他公用）
                    return 'http://01601.cn/'
                } else if (URL.indexOf("scrtvu") > 0) {//11四川干部培训
                    return 'http://px.scrtvu.net/'
                } else if (URL.indexOf("bcvet") > 0) {//12百年树人
                    return 'http://study.bcvet.com.cn/'
                }
            },




			a: "file:///Users/niesiyuan/Desktop/移动端学员角色",	  //自己
			cs: "http://cas.cs.teacheredu.cn/",			//cs
			cspub: "http://cas.cspub.teacheredu.cn/",	//cspub
			out_net: "http://cas.study.teacheredu.cn/",	//外网


			login: "mobile/v410",						//登录相关的
			long: "proj/proj/tlogin/mobile",		//远程项目相关的
			focus: "edu/proj/tlogin/mobile"		//集中项目相关的
		},
		login: {

		},
		config: {

		}
	}
};

siteconfig = allConfig.mtconfig;
export default siteconfig;


