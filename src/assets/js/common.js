//rem转化px
(function (doc, win) {
	var docEl = doc.documentElement,
		resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
		recalc = function () {
			var clientWidth = docEl.clientWidth;
			if (!clientWidth) return;
			if (clientWidth >= 750) {
				docEl.style.fontSize = '100px';
			} else {
				docEl.style.fontSize = 100 * (clientWidth / 750) + 'px';
			}
		};
	if (!doc.addEventListener) return;
	win.addEventListener(resizeEvt, recalc, false);
	doc.addEventListener('DOMContentLoaded', recalc, false);
})(document, window);

// 返回
// backUrl   均返回我/工作室
// backUrl2  均返回研修圈
// backUrl3  均返回研修详情
function back(){
	// window.history.go(-1)
	var backUrl = localStorage.getItem("backUrl")	  //我→all
													  //工作室→all
	var backUrl2 = localStorage.getItem("backUrl2")	  //研修圈→发布
	var backUrl3 = localStorage.getItem("backUrl3")   //研修圈→我的圈子→发布
	if(backUrl3){
		window.location.href = siteconfig.api.a + backUrl3
		localStorage.removeItem("backUrl3")
	}else if(backUrl2){
		window.location.href = siteconfig.api.a + backUrl2
		localStorage.removeItem("backUrl2")
	}else if(backUrl){
		window.location.href = siteconfig.api.a + backUrl
		localStorage.removeItem("backUrl")
	}else{
		window.history.go(-1)
	}
}
// 小弹窗
function xiaotanchuang(words,goWhere){
	$(".tanchuang").html(words)
    $(".tanchuang").show()
    setTimeout(function(){
		$(".tanchuang").hide()
    },1500)
}





// 时间戳
function formatDate (date, fmt) {
	// alert(date)
    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, 
                (date.getFullYear() + '').substr(4 - RegExp.$1.length));
    }
    let o = {
        'M+': date.getMonth() + 1,
        'd+': date.getDate(),
        'h+': date.getHours(),
        'm+': date.getMinutes(),
        's+': date.getSeconds()
    };
    for (let k in o) {
        if (new RegExp(`(${k})`).test(fmt)) {
            let str = o[k] + '';
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? 
            str : padLeftZero(str));
        }
	}
	// alert(date,fmt)
    return fmt;
};

function padLeftZero (str) {
    return ('00' + str).substr(str.length);
};
// 时间戳结束

// 获取当前时间
function Format(fmt,time){
    var o = {
        "M+": time.getMonth() + 1, //月份 
        "d+": time.getDate(), //日 
        "H+": time.getHours(), //小时 
        "m+": time.getMinutes(), //分 
        "s+": time.getSeconds(), //秒 
        "q+": Math.floor((time.getMonth() + 3) / 3), //季度 
        "S": time.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (time.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
};
// 点击列表进附件预览
function goFJ(fileType,name,url){        ////显示图片类型,附件名称,url路径
	this.$router.push({name:'fjPreview',query:{fileType:fileType,name:name,url:url}})  
}

// 上传附件 upImg(e,最多可选择几张)
function upImg(e,allchooseNum){
	const $target = e.$target || e.srcElement   //获取dom元素
	const files = $target.files                 //获取所有文件流(对象)
	const chooceNum = allchooseNum - this.imgArr.length  //计算当前可以选几张
	if(files.length > chooceNum){
		alert('您最多还可选择' + chooceNum + '张图片')
	}else{
		Object.keys(files).forEach(key => {
			let file = files[key]                       //获取单个文件流的值
			var data = new FormData();                  //创建formData对象,以便进行赋值传参
			data.append('file',file)                    //指定类型file
			data.append('return_uri','fileurl')         //给后台传的参数(return_uri:fileurl)
			data.append('params','subPath:C_attr;subPathRule:C_task;fileNameRule:R_12;resize:OFS')   //给后台传的参数
			let config = {                              //headers头类型
				headers: {
					'Content-Type': 'multipart/form-data'
				}
			}                      
			axios.post('/api/uploader/imgupload.do', data, config).then(res => {   //(接口地址,参数,header头类型)
				//我们是返回url地址
				this.imgArr.push(res.data)              //将返回数组扔进数组 
				$target.value = '';                     //可以传相同的图片
			})
		})
	}
}
// 地图初始化
export function ready(longitude,latitude,radius){
	var map =new BMap.Map("allmap");
	var point =new BMap.Point(longitude,latitude);
	console.log(longitude)
	console.log(latitude)
	var marker =new BMap.Marker(point);
	map.addOverlay(marker);
	map.centerAndZoom(point,15);
	map.enableScrollWheelZoom(true);

	// 圆环半径(半径大小)
	var circle = new BMap.Circle(point,radius,{fillColor:"blue", strokeWeight: 1 ,fillOpacity: 0.3, strokeOpacity: 0.3,enableEditing:false});
	map.addOverlay(circle);
	// 圆环半径结束

	var marker = new BMap.Marker(point);  // 创建标注
	marker.addEventListener("click", function(e){
		searchInfoWindow.open(marker);
	});
	map.addOverlay(marker);              // 将标注添加到地图中
	// this.isRange();
}
export{formatDate,Format,xiaotanchuang,goFJ,upImg}