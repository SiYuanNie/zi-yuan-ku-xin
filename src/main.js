// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'     //引入vuex
import mainapi from './assets/js/mainapi'
import siteconfig from './assets/js/api'

import './assets/js/jquery-3.2.1.min.js'
import './assets/js/api.js'
import './assets/js/mainapi.js'
import './assets/css/common.css'

import './assets/js/jquery-1.6.js'
import './assets/js/categoryLayer.js'
// import './assets/js/tmpl.min.js'
import './assets/js/cch5upload.js'
import './assets/css/style.css'
// import './assets/css/bootstrap.css'
import './assets/css/category.css'

Vue.config.productionTip = false
Vue.prototype.mainapi = mainapi;                //把axios对象挂到Vue原型上
Vue.prototype.$siteconfig = siteconfig;     //把axios对象挂到Vue原型上

// mint-ui
// import MintUI from 'mint-ui'
// import 'mint-ui/lib/style.css'
// Vue.use(MintUI)

// import { Popup } from 'mint-ui';
// Vue.component(Popup.name, Popup);

// vant-ui
import 'vant/lib/index.css';
import { Popup } from 'vant';                       //Popup 弹出层
import { Picker } from 'vant';
import { Dialog } from 'vant';
import { Loading  } from 'vant';  // 加载
import { Swipe, SwipeItem } from 'vant';
import { Rate } from 'vant';
import { DropdownMenu, DropdownItem, Image } from 'vant';  //下拉菜单
import { TreeSelect } from 'vant';                  //TreeSelect 分类选择
import { Icon } from 'vant';
import { Divider } from 'vant';                     //Divider 分割线
import { Toast } from 'vant';                       //Toast 轻提示
import { ActionSheet } from 'vant';                 //ActionSheet 动作面板
import { NavBar } from 'vant';                      //NavBar 导航栏
import { Skeleton } from 'vant';                    //Skeleton 骨架屏
import { Uploader,Button } from 'vant';					//Uploader 文件上传
import { RadioGroup, Radio } from 'vant';
import { List } from 'vant';
import { Tab, Tabs } from 'vant';

Vue.use(Tab);
Vue.use(Tabs);
Vue.use(List);
Vue.use(Popup);
Vue.use(Picker);
Vue.use(Dialog);
Vue.use(Loading);
Vue.use(Swipe)
Vue.use(SwipeItem)
Vue.use(Rate)
Vue.use(DropdownMenu);
Vue.use(DropdownItem);
Vue.use(TreeSelect);
Vue.use(Icon);
Vue.use(Divider);
Vue.use(Toast);
Vue.use(ActionSheet);
Vue.use(NavBar);
Vue.use(Skeleton);
Vue.use(Uploader);
Vue.use(Button);
Vue.use(Radio);
Vue.use(RadioGroup);









/* eslint-disable no-new */
new Vue({
	el: '#app',
	router,
	store,          //引入vuex
	components: { App },
	template: '<App/>'
})
